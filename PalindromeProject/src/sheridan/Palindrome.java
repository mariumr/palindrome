package sheridan;

public class Palindrome {

	public static void main(String[] args) {
		System.out.println("Is anna palindrome? " + Palindrome.isPalindrome("anna"));
		System.out.println("Is racecar palindrome? " + Palindrome.isPalindrome("racecar"));
		System.out.println("Is maham palindrome? " + Palindrome.isPalindrome("maham"));
		System.out.println("Is marium palindrome? " + Palindrome.isPalindrome("marium"));
	}
	
	public static boolean isPalindrome(String input) { 
		//Remove all lower case and remove spaces
		input = input.toLowerCase().replaceAll(" ", "");
		
		for(int i = 0 , j = input.length() -1 ; i < j ; i++ , j--) { 
			if(input.charAt(i) != input.charAt(j)) { 
				return false;
			}
		}
		  return true;
	}

}
