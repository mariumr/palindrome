package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalidrome() {
		assertTrue("Invalid input" , Palindrome.isPalindrome("maham"));
	}

	@Test
	public void testIsPalidromeException() {
		assertFalse("Invalid input" , Palindrome.isPalindrome("marium"));
	}
	
	@Test
	public void testIsPalidromeBoundryIn() {
		assertTrue("Invalid input" , Palindrome.isPalindrome("o"));
	}
	

	@Test
	public void testIsPalidromeBoundryOut() {
		assertFalse("Invalid input" , Palindrome.isPalindrome("ramnar"));
	}
}
